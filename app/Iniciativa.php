<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iniciativa extends Model
{

    protected $table = 'iniciativas';

    protected $fillable = [
        'nome','entidades','descricao','frente_principal','escopo_iniciativa','cidade','uf','link_redes','arrecadacao_virtual',
        'dados_bancarios','observacoes','resp_email_nome','resp_email_email','resp_email_telefone','resp_email_msg','contato_site',
        'contato_facebook', 'contato_instagram','contato_email','contato_telefone','contato_whatsapp','resp_iniciativa_nome','resp_iniciativa_email',
        'resp_iniciativa_telefone','status_atual','status_responsavel'
    ];

   
}
