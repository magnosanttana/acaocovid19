<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Iniciativa;

class IniciativasController extends Controller
{
    public function dashboard()
    {
        return view('base');
    }

    public function index(Request $request)
    {
        $query     = Iniciativa::orderBy('id', 'asc');
        $filtro = $request->input('filter');
        
        if($filtro){
            if($filtro['nome']){
                $query->Where('nome', 'like', '%'.$filtro['nome'].'%');
            }

            if($filtro['entidade']){
                $query->Where('entidades', 'like', '%'.$filtro['entidade'].'%');
            }

            if($filtro['cidade']){
                $query->Where('cidade', $filtro['cidade']);
            }

            if($filtro['uf']){
                $query->Where('uf', $filtro['uf']);
            }

            if($filtro['status']){
                $query->Where('status_atual', $filtro['status']);
            }

            if($filtro['responsavel']){
                $query->Where('status_responsavel', $filtro['responsavel']);
            }

            
        }else{
            
        }
        
        $registros = $query->paginate(50);

        $arrCidades    = $this->_getCidades();
        $arrUfs        = $this->_getUfs();
        $arrStatus     = $this->_getStatus();
        return view('iniciativas', [
                                    'page_titulo' => 'Iniciativas',
                                    'page_descricao' => '',
                                    'cidades' => $arrCidades,
                                    'ufs'    => $arrUfs,
                                    'status'    => $arrStatus,
                                    'result' => ['registros' => []],
                                    'registros' => $registros,
                                    ]);
    }

    public function new()
    {
        return view('iniciativas-novo', [
            'page_titulo' => 'Iniciativas Cadastradas',
            'page_descricao' => 'Novo Cadastro',
            'cidades' => [],
            'ufs'    => [],
            'result' => ['registros' => []],
            'registros' => [],
            ]);
    }

    public function save(Request $request)
    {
        $dados = $request->input('iniciativa');

        if($request->input('id')){
            Iniciativa::where('id', $request->input('id'))
            ->update($dados);
        }else{
            Iniciativa::create($dados);
        }

        return redirect('/iniciativas');
        
        dd('falha');
        
    }

    public function editar($id, Request $request)
    {
        $registro = Iniciativa::find($id);
        if(!$registro){
            return redirect('/iniciativas');
        }

        return view('iniciativas-edicao', [
            'page_titulo' => 'Iniciativas Cadastradas',
            'page_descricao' => 'Edição',
            'cidades' => [],
            'ufs'    => [],
            'result' => ['registros' => []],
            'registro' => $registro,
            ]);
    }

    private function _getCidades(){
        $registros  = Iniciativa::select('cidade')->distinct()->orderBy('cidade', 'asc')->get();
        $arrCidades = [];
        foreach($registros as $registro){
            $arrCidades[] = $registro->cidade;
        }
        return $arrCidades;
    }

    private function _getUfs(){
        $registros  = Iniciativa::select('uf')->distinct()->orderBy('uf', 'asc')->get();
        $arrUfs = [];
        foreach($registros as $registro){
            $arrUfs[] = $registro->uf;
        }
        return $arrUfs;
    }

    private function _getStatus(){
        $registros  = Iniciativa::select('status_atual')->distinct()->orderBy('status_atual', 'asc')->get();
        $arrStatus = [];
        foreach($registros as $registro){
            $arrStatus[] = $registro->status_atual;
        }
        return $arrStatus;
    }
    
}
