<?php

namespace App\Helpers;

if (!function_exists('oldCustom')) {
    function oldCustom($name)
    {
        $input = explode('.', $name);
        if (count($input) > 1) {
            return $_REQUEST[$input[0]][$input[1]] ?? '';
        } else {
            return $_REQUEST[$input[0]] ?? '';
        }
    }
}