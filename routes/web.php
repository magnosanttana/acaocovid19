<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Iniciativa;

 Route::get('/import', function () {
    //$contents = explode("\n", \Storage::disk('public')->get('iniciativas.csv')); 
    echo "<pre>";
    //dd(substr("abcdef", 0, 2));
    if (($handle = fopen("iniciativas1.csv", "r")) !== FALSE) {
        $i = 0;
        while (($linha = fgetcsv($handle)) !== FALSE) {
            
            if($i >= 0){
                $dados['nome'] = $linha[0];
                $dados['entidades'] = $linha[1];
                $dados['descricao'] = $linha[2];
                $dados['frente_principal'] = $linha[3];
                $dados['escopo_iniciativa'] = strtoupper($linha[4]);
                $dados['cidade'] = $linha[5];
                $dados['uf'] = strtoupper(substr(trim($linha[6]), 0, 2));
                $dados['link_redes'] = $linha[7];
                $dados['arrecadacao_virtual'] = $linha[8];
                $dados['dados_bancarios'] = $linha[9];

                //Iniciativa::create($dados);

                //print_r($dados);
            }

            $i++;
        }
        fclose($handle);
    }

    echo "</pre>";



    //dd(str_getcsv(file_get_contents('iniciativas2.csv')));
    //$contents = explode("\n", \Storage::disk('public')->get('iniciativas.csv')); 
    //dd($contents);
    return view('base');
 });

Route::get('/', 'IniciativasController@dashboard');
Route::get('/iniciativas', 'IniciativasController@index');
Route::get('/iniciativas/novo', 'IniciativasController@new');
Route::post('/iniciativas/salvar', 'IniciativasController@save');
Route::get('/iniciativas/editar/{id}', 'IniciativasController@editar');
