@extends('base')
@section('content')
    <?php 
   // $configAlert = json_decode(base64_decode($_GET['ca']), true);
?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary ">
            <div class="box-header with-border">
            <h3 class="box-title">Edição da iniciativa #{{$registro->id}} </h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="/iniciativas/salvar" method="POST">
            @csrf
            <input type="hidden" class="form-control" name="id" value="{{$registro->id}}">
            <div class="box-body">
                <!-- <div class="form-group has-success">
                    <label class="control-label" for="inputSuccess"> Titulo</label>
                    <input type="text" class="form-control" id="inputSuccess" placeholder="O Município" readonly>
                    <span class="help-block">Help block with success</span>
                </div>
               
                <div class="alert alert-warning alert-dismissible" style="display: 'none' ;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Atenção!</h4>
                    <?php 
                        //foreach($configAlert['msgs'] as $msg){
                          //  echo"<p> - {$msg}</p>";
                        //}
                    ?>
                </div>-->

                <div class="panel box ">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                        Email original
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Responsável pelo envio do email</h4>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="resp_email_nome"> Nome</label>
                                    <input type="text" class="form-control" id="resp_email_nome" name="iniciativa[resp_email_nome]" placeholder="Nome" value="{{$registro->resp_email_nome}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="resp_email_email"> Email</label>
                                    <input type="email" class="form-control" id="resp_email_email" name="iniciativa[resp_email_email]" placeholder="Email" value="{{$registro->resp_email_email}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="resp_email_telefone"> Telefone</label>
                                    <input type="text" class="form-control" id="" name="iniciativa[resp_email_telefone]" placeholder="Com DDD" value="{{$registro->resp_email_telefone}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="editor2"> Mensagem recebida</label><br/>
                                    <textarea id="editor2" class="editor" name="iniciativa[resp_email_msg]" rows="10" cols="80">
                                    {{$registro->resp_email_msg}}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="editor2"> Tinha anexo (fotos, PDF...) no email?</label><br/>
                                   Coloque na pasta <b>"anexos#1"</b> aqui <a href="https://drive.google.com/drive/folders/1fJIHfR3JzB75aX80mqVV_9FWhPRm3Zn1?usp=sharing" target="_blank">neste link</a>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                  </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="nome"> Nome da Iniciativa</label>
                            <input type="text" class="form-control" id="nome" name="iniciativa[nome]" placeholder="Nome" value="{{$registro->nome}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="entidades"> Entidades</label>
                            <input type="text" class="form-control" id="entidades" name="iniciativa[entidades]" placeholder="Separar por vígula caso seja mais de um" value="{{$registro->entidades}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label" for="editor1"> Descrição</label><br/>
                             <textarea id="editor1" class="editor" name="iniciativa[descricao]" rows="10" cols="80">
                             {{$registro->descricao}}
                            </textarea>
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-12">
                       <div class="form-group">
                            <label class="control-label" for="frente_principal"> Frente Principal</label>
                            <input type="text" class="form-control" id="frente_principal" name="iniciativa[frente_principal]" placeholder="" value="{{$registro->frente_principal}}">
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label" for="cep"> Escopo da Iniciativa</label>
                            
                            <div class="radio">
                                <label>
                                <input type="radio" name="iniciativa[escopo_iniciativa]" id="optionsRadios1" value="MUNICIPAL" <?php if($registro->escopo_iniciativa == 'MUNICIPAL') echo 'checked'; ?> >
                                Municipal
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                <input type="radio" name="iniciativa[escopo_iniciativa]" id="optionsRadios2" value="ESTADUAL" <?php if($registro->escopo_iniciativa == 'ESTADUAL') echo 'checked'; ?> >
                                Estadual
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                <input type="radio" name="iniciativa[escopo_iniciativa]" id="optionsRadios3" value="NACIONAL" <?php if($registro->escopo_iniciativa == 'NACIONAL') echo 'checked'; ?> >
                                Nacional
                                </label>
                            </div>
                        </div>
                    </div>
                   <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label" for="cidade"> Cidade</label>
                            <input type="text" class="form-control" id="cidade" name="iniciativa[cidade]" placeholder="Cidade" value="{{$registro->cidade}}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label" for="uf"> UF </label>
                            <select class="form-control" id="uf" name="iniciativa[uf]">
                                <option value="AC" <?php if($registro->uf == 'AC') echo 'selected'; ?> >AC</option>
                                <option value="AL" <?php if($registro->uf == 'AL') echo 'selected'; ?> >AL</option>
                                <option value="AP" <?php if($registro->uf == 'AP') echo 'selected'; ?> >AP</option>
                                <option value="AM" <?php if($registro->uf == 'AM') echo 'selected'; ?> >AM</option>
                                <option value="BA" <?php if($registro->uf == 'BA') echo 'selected'; ?> >BA</option>
                                <option value="CE" <?php if($registro->uf == 'CE') echo 'selected'; ?> >CE</option>
                                <option value="DF" <?php if($registro->uf == 'DF') echo 'selected'; ?> >DF</option>
                                <option value="ES" <?php if($registro->uf == 'ES') echo 'selected'; ?> >ES</option>
                                <option value="GO" <?php if($registro->uf == 'GO') echo 'selected'; ?> >GO</option>
                                <option value="MA" <?php if($registro->uf == 'MA') echo 'selected'; ?> >MA</option>
                                <option value="MT" <?php if($registro->uf == 'MT') echo 'selected'; ?> >MT</option>
                                <option value="MS" <?php if($registro->uf == 'MS') echo 'selected'; ?> >MS</option>
                                <option value="MG" <?php if($registro->uf == 'MG') echo 'selected'; ?> >MG</option>
                                <option value="PA" <?php if($registro->uf == 'PA') echo 'selected'; ?> >PA</option>
                                <option value="PB" <?php if($registro->uf == 'PB') echo 'selected'; ?> >PB</option>
                                <option value="PR" <?php if($registro->uf == 'PR') echo 'selected'; ?> >PR</option>
                                <option value="PE" <?php if($registro->uf == 'PE') echo 'selected'; ?> >PE</option>
                                <option value="PI" <?php if($registro->uf == 'PI') echo 'selected'; ?> >PI</option>
                                <option value="RJ" <?php if($registro->uf == 'RJ') echo 'selected'; ?> >RJ</option>
                                <option value="RN" <?php if($registro->uf == 'RN') echo 'selected'; ?> >RN</option>
                                <option value="RS" <?php if($registro->uf == 'RS') echo 'selected'; ?> >RS</option>
                                <option value="RO" <?php if($registro->uf == 'RO') echo 'selected'; ?> >RO</option>
                                <option value="RR" <?php if($registro->uf == 'RR') echo 'selected'; ?> >RR</option>
                                <option value="SC" <?php if($registro->uf == 'SC') echo 'selected'; ?> >SC</option>
                                <option value="SP" <?php if($registro->uf == 'SP') echo 'selected'; ?> >SP</option>
                                <option value="SE" <?php if($registro->uf == 'SE') echo 'selected'; ?> >SE</option>
                                <option value="TO" <?php if($registro->uf == 'TO') echo 'selected'; ?> >TO</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Formas de contato da Iniciativa, caso exista, OU da Entidade Responsável</h4>
                        <input type="text" class="form-control" value="{{$registro->link_redes}}" readonly>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="contato_site"> Site</label>
                            <input type="text" class="form-control" id="contato_site" name="iniciativa[contato_site]" placeholder="Endereço Completo" value="{{$registro->contato_site}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="contato_facebook"> Facebook</label>
                            <input type="text" class="form-control" id="contato_facebook" name="iniciativa[contato_facebook]" placeholder="URL Completa: https://facebook.com/nomeDoUsuario" value="{{$registro->contato_facebook}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                       <div class="form-group">
                            <label class="control-label" for="contato_instagram"> Instagram</label>
                            <input type="text" class="form-control" id="contato_instagram" name="iniciativa[contato_instagram]" placeholder="URL Completa: https://instagram.com/nomeDoUsuario" value="{{$registro->contato_instagram}}">
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="contato_email"> Email</label>
                            <input type="email" class="form-control" id="contato_email" name="iniciativa[contato_email]" placeholder="Email" value="{{$registro->contato_email}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="contato_telefone"> Telefone</label>
                            <input type="text" class="form-control" id="contato_telefone" name="iniciativa[contato_telefone]" placeholder="Telefone" value="{{$registro->contato_telefone}}">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="contato_whatsapp"> WhatsApp</label>
                            <input type="text" class="form-control" id="contato_whatsapp" name="iniciativa[contato_whatsapp]" placeholder="Com DDD" value="{{$registro->contato_whatsapp}}">
                             <!-- <p class="help-block">Example block-level help text here.</p> -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4>Contribuições monetárias</h4>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label" for="arrecadacao_virtual"> Vaquinha Virtual</label>
                            <input type="text" class="form-control" id="arrecadacao_virtual" name="iniciativa[arrecadacao_virtual]" placeholder="Email" value="{{$registro->arrecadacao_virtual}}">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label" for="dados_bancarios"> Dados bancários</label><br/>
                           <textarea id="editor22" name="iniciativa[dados_bancarios]" rows="4" cols="70">{{$registro->dados_bancarios}}</textarea>
                        </div>
                    </div>
                   
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4>Contato do Responsável da Iniciativa ou Entidade</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="resp_iniciativa_nome"> Nome</label>
                            <input type="text" class="form-control" id="resp_iniciativa_nome" name="iniciativa[resp_iniciativa_nome]" placeholder="Telefone" value="{{$registro->resp_iniciativa_nome}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="resp_iniciativa_email"> Email</label>
                            <input type="email" class="form-control" id="resp_iniciativa_email" name="iniciativa[resp_iniciativa_email]" placeholder="Email" value="{{$registro->resp_iniciativa_email}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="resp_iniciativa_telefone"> Telefone</label>
                            <input type="text" class="form-control" id="resp_iniciativa_telefone" name="iniciativa[resp_iniciativa_telefone]" placeholder="Com DDD" value="{{$registro->resp_iniciativa_telefone}}">
                             <!-- <p class="help-block">Example block-level help text here.</p> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                <hr/>
                <div class="col-md-12">
                        <h4>Sobre o tratamento das informações desta iniciativa</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="uf"> Status atual </label>
                            <select class="form-control" id="uf" name="iniciativa[status_atual]">
                                <option value="Sem tratamento" <?php if($registro->status_atual == 'Sem tratamento') echo 'selected'; ?> >Sem tratamento</option>
                                <option value="Complementando Info" <?php if($registro->status_atual == 'Complementando Info') echo 'selected'; ?> >Complementando Info</option>
                                <option value="Primeira Revisão" <?php if($registro->status_atual == 'Primeira Revisão') echo 'selected'; ?> >Primeira Revisão</option>
                                <option value="Revisão Final" <?php if($registro->status_atual == 'Revisão Final') echo 'selected'; ?> >Revisão Final</option>
                                <option value="Finalizado" <?php if($registro->status_atual == 'Finalizado') echo 'selected'; ?> >Finalizado</option>
                            </select>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="status_responsavel"> Responsável</label>
                            <input type="text" class="form-control" id="status_responsavel" name="iniciativa[status_responsavel]" placeholder="Nome" value="{{$registro->status_responsavel}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label" for="nome"> Histórico/Observações</label><br/>
                             <textarea id="editor3" class="editor" name="iniciativa[observacoes]" rows="10" cols="80">
                             {{$registro->observacoes}}
                            </textarea>
                        </div>
                    </div>
                </div>
                
                <!--
                <div class="checkbox">
                <label>
                    <input type="checkbox" name="cartorio[ativo]" value="1"> Cartório Ativo
                </label>
                </div>-->
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Salvar</button>
                <a href="/iniciativas" class="btn btn-default">Cancelar</a>
            </div>
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>


<?php //include_once(__DIR__."/modal-agendamento.php"); ?>
@endsection