@extends('base')
@section('content')
    <?php 
   // $configAlert = json_decode(base64_decode($_GET['ca']), true);
?>
<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary ">
            <div class="box-header with-border">
            <h3 class="box-title">Novo Cadastro</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="/iniciativas/salvar" method="POST">
            @csrf
            <div class="box-body">
                <!-- <div class="form-group has-success">
                    <label class="control-label" for="inputSuccess"> Titulo</label>
                    <input type="text" class="form-control" id="inputSuccess" placeholder="O Município" readonly>
                    <span class="help-block">Help block with success</span>
                </div>
               
                <div class="alert alert-warning alert-dismissible" style="display: 'none' ;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Atenção!</h4>
                    <?php 
                        //foreach($configAlert['msgs'] as $msg){
                          //  echo"<p> - {$msg}</p>";
                        //}
                    ?>
                </div>-->

                <div class="panel box ">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                        Email original
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Responsável pelo envio do email</h4>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="resp_email_nome"> Nome</label>
                                    <input type="text" class="form-control" id="resp_email_nome" name="iniciativa[resp_email_nome]" placeholder="Nome" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="resp_email_email"> Email</label>
                                    <input type="email" class="form-control" id="resp_email_email" name="iniciativa[resp_email_email]" placeholder="Email" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="resp_email_telefone"> Telefone</label>
                                    <input type="text" class="form-control" id="" name="iniciativa[resp_email_telefone]" placeholder="Com DDD" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="editor2"> Mensagem recebida</label><br/>
                                    <textarea id="editor2" class="editor" name="iniciativa[resp_email_msg]" rows="10" cols="80">
                                    Colar conteudo do email aqui
                                    </textarea>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                  </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="nome"> Nome da Iniciativa</label>
                            <input type="text" class="form-control" id="nome" name="iniciativa[nome]" placeholder="Nome" value="<?= old('cartorio.nome');?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="entidades"> Entidades</label>
                            <input type="text" class="form-control" id="entidades" name="iniciativa[entidades]" placeholder="Separar por vígula caso seja mais de um" value="<?= old('cartorio.razao');?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label" for="editor1"> Descrição</label><br/>
                             <textarea id="editor1" class="editor" name="iniciativa[descricao]" rows="10" cols="80">
                            </textarea>
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-12">
                       <div class="form-group">
                            <label class="control-label" for="frente_principal"> Frente Principal</label>
                            <input type="text" class="form-control" id="frente_principal" name="iniciativa[frente_principal]" placeholder="">
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label" for="cep"> Escopo da Iniciativa</label>
                            
                            <div class="radio">
                                <label>
                                <input type="radio" name="iniciativa[escopo_iniciativa]" id="optionsRadios1" value="MUNICIPAL" checked>
                                Municipal
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                <input type="radio" name="iniciativa[escopo_iniciativa]" id="optionsRadios2" value="ESTADUAL">
                                Estadual
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                <input type="radio" name="iniciativa[escopo_iniciativa]" id="optionsRadios3" value="NACIONAL">
                                Nacional
                                </label>
                            </div>
                        </div>
                    </div>
                   <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label" for="cidade"> Cidade</label>
                            <input type="text" class="form-control" id="cidade" name="iniciativa[cidade]" placeholder="Cidade" value="">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label" for="uf"> UF </label>
                            <select class="form-control" id="uf" name="iniciativa[uf]">
                                <option value="AC" <?php if(old('cartorio.uf') == 'AC') echo 'selected'; ?> >AC</option>
                                <option value="AL" <?php if(old('cartorio.uf') == 'AL') echo 'selected'; ?> >AL</option>
                                <option value="AP" <?php if(old('cartorio.uf') == 'AP') echo 'selected'; ?> >AP</option>
                                <option value="AM" <?php if(old('cartorio.uf') == 'AM') echo 'selected'; ?> >AM</option>
                                <option value="BA" <?php if(old('cartorio.uf') == 'BA') echo 'selected'; ?> >BA</option>
                                <option value="CE" <?php if(old('cartorio.uf') == 'CE') echo 'selected'; ?> >CE</option>
                                <option value="DF" <?php if(old('cartorio.uf') == 'DF') echo 'selected'; ?> >DF</option>
                                <option value="ES" <?php if(old('cartorio.uf') == 'ES') echo 'selected'; ?> >ES</option>
                                <option value="GO" <?php if(old('cartorio.uf') == 'GO') echo 'selected'; ?> >GO</option>
                                <option value="MA" <?php if(old('cartorio.uf') == 'MA') echo 'selected'; ?> >MA</option>
                                <option value="MT" <?php if(old('cartorio.uf') == 'MT') echo 'selected'; ?> >MT</option>
                                <option value="MS" <?php if(old('cartorio.uf') == 'MS') echo 'selected'; ?> >MS</option>
                                <option value="MG" <?php if(old('cartorio.uf') == 'MG') echo 'selected'; ?> >MG</option>
                                <option value="PA" <?php if(old('cartorio.uf') == 'PA') echo 'selected'; ?> >PA</option>
                                <option value="PB" <?php if(old('cartorio.uf') == 'PB') echo 'selected'; ?> >PB</option>
                                <option value="PR" <?php if(old('cartorio.uf') == 'PR') echo 'selected'; ?> >PR</option>
                                <option value="PE" <?php if(old('cartorio.uf') == 'PE') echo 'selected'; ?> >PE</option>
                                <option value="PI" <?php if(old('cartorio.uf') == 'PI') echo 'selected'; ?> >PI</option>
                                <option value="RJ" <?php if(old('cartorio.uf') == 'RJ') echo 'selected'; ?> >RJ</option>
                                <option value="RN" <?php if(old('cartorio.uf') == 'RN') echo 'selected'; ?> >RN</option>
                                <option value="RS" <?php if(old('cartorio.uf') == 'RS') echo 'selected'; ?> >RS</option>
                                <option value="RO" <?php if(old('cartorio.uf') == 'RO') echo 'selected'; ?> >RO</option>
                                <option value="RR" <?php if(old('cartorio.uf') == 'RR') echo 'selected'; ?> >RR</option>
                                <option value="SC" <?php if(old('cartorio.uf') == 'SC') echo 'selected'; ?> >SC</option>
                                <option value="SP" <?php if(old('cartorio.uf') == 'SP') echo 'selected'; ?> >SP</option>
                                <option value="SE" <?php if(old('cartorio.uf') == 'SE') echo 'selected'; ?> >SE</option>
                                <option value="TO" <?php if(old('cartorio.uf') == 'TO') echo 'selected'; ?> >TO</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Formas de contato da Iniciativa, caso exista, OU da Entidade Responsável</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="contato_site"> Site</label>
                            <input type="text" class="form-control" id="contato_site" name="iniciativa[contato_site]" placeholder="Endereço Completo" value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="contato_facebook"> Facebook</label>
                            <input type="text" class="form-control" id="contato_facebook" name="iniciativa[contato_facebook]" placeholder="URL Completa: https://facebook.com/nomeDoUsuario" value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                       <div class="form-group">
                            <label class="control-label" for="contato_instagram"> Instagram</label>
                            <input type="text" class="form-control" id="contato_instagram" name="iniciativa[contato_instagram]" placeholder="URL Completa: https://instagram.com/nomeDoUsuario" value="">
                        </div>
                    </div>
                </div>
               
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="contato_email"> Email</label>
                            <input type="email" class="form-control" id="contato_email" name="iniciativa[contato_email]" placeholder="Email" value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="contato_telefone"> Telefone</label>
                            <input type="text" class="form-control" id="contato_telefone" name="iniciativa[contato_telefone]" placeholder="Telefone" value="">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="contato_whatsapp"> WhatsApp</label>
                            <input type="text" class="form-control" id="contato_whatsapp" name="iniciativa[contato_whatsapp]" placeholder="Com DDD" value="">
                             <!-- <p class="help-block">Example block-level help text here.</p> -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4>Contribuições monetárias</h4>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label class="control-label" for="arrecadacao_virtual"> Vaquinha Virtual</label>
                            <input type="text" class="form-control" id="arrecadacao_virtual" name="iniciativa[arrecadacao_virtual]" placeholder="Email" value="">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label class="control-label" for="dados_bancarios"> Dados bancários</label><br/>
                           <textarea id="editor22" name="iniciativa[dados_bancarios]" rows="4" cols="70"></textarea>
                        </div>
                    </div>
                   
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4>Contato do Responsável da Iniciativa ou Entidade</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="resp_iniciativa_nome"> Nome</label>
                            <input type="text" class="form-control" id="resp_iniciativa_nome" name="iniciativa[resp_iniciativa_nome]" placeholder="Telefone" value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="resp_iniciativa_email"> Email</label>
                            <input type="email" class="form-control" id="resp_iniciativa_email" name="iniciativa[resp_iniciativa_email]" placeholder="Email" value="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="resp_iniciativa_telefone"> Telefone</label>
                            <input type="text" class="form-control" id="resp_iniciativa_telefone" name="iniciativa[resp_iniciativa_telefone]" placeholder="Com DDD" value="">
                             <!-- <p class="help-block">Example block-level help text here.</p> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                <hr/>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="uf"> Status atual </label>
                            <select class="form-control" id="uf" name="iniciativa[status_atual]">
                                <option value="Sem tratamento">Sem Tratamento</option>
                                <option value="Complementando Info">Complementando Info</option>
                                <option value="Primeira Revisão">Primeira Revisão</option>
                                <option value="Revisão Final">Revisão Final</option>
                                <option value="Finalizado" >Finalizado</option>
                            </select>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label" for="status_responsavel"> Responsável</label>
                            <input type="text" class="form-control" id="status_responsavel" name="iniciativa[status_responsavel]" placeholder="Nome" value="<?= old('cartorio.cidade');?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label" for="nome"> Histórico/Observações</label><br/>
                             <textarea id="editor3" class="editor" name="iniciativa[observacoes]" rows="10" cols="80">
                            </textarea>
                        </div>
                    </div>
                </div>
                
                <!--
                <div class="checkbox">
                <label>
                    <input type="checkbox" name="cartorio[ativo]" value="1"> Cartório Ativo
                </label>
                </div>-->
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Salvar</button>
                <a href="/iniciativas" class="btn btn-default">Cancelar</a>
            </div>
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>


<?php //include_once(__DIR__."/modal-agendamento.php"); ?>
@endsection