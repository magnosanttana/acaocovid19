@extends('base')
@section('content')
 <?php 
        function oldCustom($name)
        {
            $input = explode('.', $name);
            if (count($input) > 1) {
                return $_REQUEST[$input[0]][$input[1]] ?? '';
            } else {
                return $_REQUEST[$input[0]] ?? '';
            }
        }
?>
<div class="row">
                
    <div class="col-xs-12">
         <div class="alert alert-danger alert-dismissible" style="display: none">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-info"></i> Atenção!</h4>
            <?php 
              //  foreach($configAlert['msgs'] as $msg){
                //    echo"<p> {$msg}</p>";
               // }
            ?>
        </div>
        <div class="alert alert-success alert-dismissible" style="display: none">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-info"></i> Atenção!</h4>
            <?php 
                //foreach($configAlert['msgs'] as $msg){
                  //  echo"<p> {$msg}</p>";
                //}
            ?>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pesquisar</h3>
                       <!-- <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                            </button>
                        </div>-->
                    </div>
                   
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form">
                    <div class="box-body">
                        <div class="form-group col-xs-2">
                            <label for="exampleInputEmail1">Iniciativa</label>
                            <input type="text" name="filter[nome]" class="form-control" id="exampleInputEmail1" placeholder="Nome" value="<?=oldCustom('filter.nome')?>">
                        </div>
                         <div class="form-group col-xs-2">
                            <label for="exampleInputEmail1">Entidade</label>
                            <input type="text" name="filter[entidade]" class="form-control" id="exampleInputEmail1" placeholder="Entidade" value="<?=oldCustom('filter.entidade')?>">
                        </div>
                        <div class="form-group col-xs-2">
                            <label for="exampleInputEmail1">Cidade</label>
                            <select class="form-control" name="filter[cidade]">
                                <option value="">Todos</option>
                                <?php 
                                foreach ($cidades as $cidade): ?>
                                    <option 
                                    <?php if(oldCustom('filter.cidade') == $cidade): ?>
                                    selected 
                                <?php endif;?>
                                    value="<?=$cidade?>"><?=$cidade?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group col-xs-2">
                            <label>UF</label>
                            <select class="form-control" name="filter[uf]">
                                <option value="">Todos</option>
                                <?php 
                                foreach ($ufs as $uf): ?>
                                    <option 
                                    <?php if(oldCustom('filter.uf') == $uf): ?>
                                    selected 
                                <?php endif;?>
                                    value="<?=$uf?>"><?=$uf?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                       
                        <div class="form-group col-xs-2">
                            <label>Status Atual</label>
                            <select class="form-control" name="filter[status]">
                                <option value="">Todos</option>
                               
                                <?php 
                                foreach ($status as $st): ?>
                                    <option 
                                    <?php if(oldCustom('filter.status') == $st): ?>
                                    selected 
                                <?php endif;?>
                                    value="<?=$st?>"><?=$st?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                         <div class="form-group col-xs-2">
                            <label>Resp pelo status</label>
                            <input type="text" name="filter[responsavel]" class="form-control" id="exampleInputEmail1" placeholder="Nome" value="<?=oldCustom('filter.responsavel')?>">
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Pesquisar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="box box-success">
        
        <div class="box-header">
            <h3 class="box-title">Iniciativas cadastradas ({{$registros->total()}})</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">

                        <div class="input-group-btn">
                        <a href="/iniciativas/novo" class="btn btn-success"><i class="fa fa-plus-circle"></i> Cadastrar Nova</a>
                        <!-- <a href="/cartorios/importar-xml" class="btn btn-success"><i class="fa fa-upload"></i> Importar XML</a>
                        <a href="#" class="btn btn-info btn-agendamento" data-toggle="modal" data-target="#modal-agendamento-email"><i class="fa fa-envelope"></i> Enviar Email</a>
                        -->
                        </div>
                    </div>
                </div> 
        </div>
        <!-- /.box-header -->

       

       
        <div class="box-body table-responsive no-padding">
            <?php if(count($registros)): ?>
            <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>ID</th>
                <th style="width: 35%">Iniciativa/Entidade</th>
                <th>Cidade/UF</th>
                <th>Status</th>
                <th>Responsavel</th>
                <th style="width: 15%">Ações</th>
            </tr>
            <?php $i = 1; foreach($registros as $registro): ?>
            <tr>
                <td>{{$i}}</td>
                <td>{{$registro->id}}</td>
                <td>{{$registro->nome}} / {{$registro->entidades}}</td>
                <td>{{$registro->cidade}}/{{$registro->uf}}</td>
                <td>
                    <?php 
                        if($registro->status_atual == 'Sem tratamento'){
                            echo '<span class="label label-danger">Sem tratamento</span>';
                        }elseif($registro->status_atual == 'Finalizado'){
                            echo '<span class="label label-success">Finalizado</span>';
                        }else{
                            echo '<span class="label label-warning">'.$registro->status_atual.'</span>';
                        }
                    ?>
                </td>
                <td>
                   {{$registro->status_responsavel}}
                </td>
                <td>
                    <a href="/iniciativas/editar/{{$registro->id}}" class="btn btn-default"> <i class="fa fa-edit"></i> Editar</a>
                </td>
            </tr>
            <?php $i++; endforeach; ?>
           
            </table>
            <?php endif; ?>
              <!--  <div class="col-xs-12">
                    <a href="/cartorios/exportar-xls?<?php echo $_SERVER['QUERY_STRING'];?>" target="_blank" class="btn btn-primary pull-right"><i class="fa fa-download"></i> Exportar resultado (XLS)</a><br/><br/>
                </div> -->
        </div>
        <!-- /.box-body -->
            <div class="box-footer clearfix">
            <?php 
                $filter = [];
                if(isset($_REQUEST['filter'])){
                    $filter['filter'] = $_REQUEST['filter'];
                }
            ?>
            {{$registros->appends($filter)->links()}}
              <?php //echo paginator($_GET['page'] ?? 1, $result['total_pages']); ?>
              
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>


<div class="modal fade" id="modal-default" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Confirmação de Exclusão</h4>
        </div>
        <div class="modal-body">
            <p>Deseja realmente excluir este registro? Esta ação não poderá ser desfeita.…</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-danger" id="btn-confirma-exclusao" data-entidade="" data-id="" data-dismiss="modal">Sim, Excluir</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<?php //include_once(__DIR__."/modal-agendamento.php"); ?>
@endsection